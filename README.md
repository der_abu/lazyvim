# 💤 LazyVim

Modified from the starter template for [LazyVim](https://github.com/LazyVim/LazyVim).
Refer to the [documentation](https://lazyvim.github.io/installation) to get started.

## Pre-Installation

```
rm -rf ~/.config/nvim
rm -rf ~/.local/share/nvim
rm -rf ~/.local/state/nvim
rm -rf ~/.cache/nvim
```

## Installation

```
git clone https://gitlab.com/der_abu/lazyvim.git ~/.config/nvim
```

## Help

<https://lazyvim-ambitious-devs.phillips.codes/>
