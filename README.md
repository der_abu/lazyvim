# 💤 LazyVim

Modified from the starter template for [LazyVim](https://github.com/LazyVim/LazyVim).
Refer to the [documentation](https://lazyvim.github.io/installation) to get started.

## Pre-Installation

```
rm -rf ~/.config/nvim
rm -rf ~/.local/share/nvim
rm -rf ~/.local/state/nvim
rm -rf ~/.cache/nvim
```

## Installation

### for everyone else

```
git clone https://gitlab.com/der_abu/lazyvim.git ~/.config/nvim
rm -rf ~/.config/nvim/.git
```

### for me

```
git clone git@gitlab.com:der_abu/lazyvim.git ~/.config/nvim
```

## Help

<https://lazyvim-ambitious-devs.phillips.codes/>

## Cheatsheet

### yanky

- Press ]y and [y to cycle through pastes (p first)
- Press (leader)p to list the paste buffer

### mini.files

- ```<leader>fm``` to open

### mini.diff

- ```<leader>go``` Toggle overlay
