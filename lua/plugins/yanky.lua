return {
  "gbprod/yanky.nvim",
  keys = {
    { "<c-p>", "<Plug>(YankyPreviousEntry)", mode = { "n", "x" }, desc = "Cycle previous yank" },
    { "<c-n>", "<Plug>(YankyNextEntry)", mode = { "n", "x" }, desc = "Cycle next yank" },
  },
}
