return {
  "monaqa/dial.nvim",
  opts = function(_, opts)
    local augend = require("dial.augend")

    -- visibility
    local visibility = augend.constant.new({
      elements = {
        "public",
        "protected",
        "private",
      },
      word = true,
      cyclic = true,
    })
    table.insert(opts.groups.default, visibility)

    -- misc
    table.insert(opts.groups.default, augend.date.alias["%d.%m.%Y"])
    table.insert(opts.groups.default, augend.date.alias["%H:%M"])
    table.insert(opts.groups.default, augend.constant.alias.de_weekday)
    table.insert(opts.groups.default, augend.constant.alias.de_weekday_full)
    table.insert(opts.groups.default, augend.semver.alias.semver)
  end,
}
