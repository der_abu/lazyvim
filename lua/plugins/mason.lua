return {
  "williamboman/mason.nvim",
  opts = {
    ensure_installed = {
      "twiggy-language-server",
      "curlylint",
      "shellcheck",
      "shfmt",
      "stylua",
    },
  },
}
