return {
  "williamboman/mason.nvim",
  opts = {
    ensure_installed = {
      "emmet-language-server",
      "twiggy-language-server",
      "curlylint",
      "shellcheck",
      "shfmt",
      "stylua",
    },
  },
}
