return {
  "stevearc/conform.nvim",
  opts = {
    formatters = {
      ddev_php_cs_fixer = {
        command = "tools/ddev-bridge/php-cs-fixer",
        args = { "$FILENAME" },
        stdin = false,
      },
    },
    formatters_by_ft = {
      -- this uses your systems php version
      -- php = { "php_cs_fixer" },
      --
      -- intelephense decides how this works
      -- php = { "intelephense" },
      --
      -- this uses the php version from your ddev container
      php = { "ddev_php_cs_fixer" },
    },
  },
}
