local function mirror(args)
  return args[1][1]
end

local function mirrorTitleCase(args)
  return (args[1][1]:gsub("^%l", string.upper))
end

return {
  s(
    "set",
    fmt(
      [[
public function set{}({} ${}): void
{{
  $this->{} = ${};
}}

    ]],
      {
        f(mirrorTitleCase, { 1 }),
        i(2),
        i(1),
        f(mirror, { 1 }),
        f(mirror, { 1 }),
      }
    )
  ),
}
