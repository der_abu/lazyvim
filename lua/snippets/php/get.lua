local function mirrorTitleCase(args)
  return (args[1][1]:gsub("^%l", string.upper))
end

return {
  s(
    "get",
    fmt(
      [[
public function get{}(): {}
{{
  return $this->{};
}}

    ]],
      {
        f(mirrorTitleCase, { 1 }),
        i(2),
        i(1),
      }
    )
  ),
}
