local function mirror(args)
  return args[1][1]
end

local function mirrorTitleCase(args)
  return (args[1][1]:gsub("^%l", string.upper))
end

return {
  s(
    "setget",
    fmt(
      [[
public function set{}({} ${}): void
{{
  $this->{} = ${};
}}

public function get{}(): {}
{{
  return $this->{};
}}

    ]],
      {
        f(mirrorTitleCase, { 1 }),
        i(2),
        i(1),
        f(mirror, { 1 }),
        f(mirror, { 1 }),
        f(mirrorTitleCase, { 1 }),
        f(mirror, { 2 }),
        f(mirror, { 1 }),
      }
    )
  ),
}
