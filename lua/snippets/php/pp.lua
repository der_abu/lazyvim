local function mirror(args)
  return args[1][1]
end

local function mirrorTitleCase(args)
  return (args[1][1]:gsub("^%l", string.upper))
end

return {
  s(
    "pp",
    fmt(
      [[
    private {} ${};

    public function set{}({} ${}): void
    {{
        $this->{} = ${};
    }}

    public function get{}(): {}
    {{
        return $this->{};
    }}

    ]],
      {
        -- type
        i(1),
        -- name
        i(2),
        f(mirrorTitleCase, { 2 }),
        f(mirror, { 1 }),
        f(mirror, { 2 }),
        f(mirror, { 2 }),
        f(mirror, { 2 }),
        f(mirrorTitleCase, { 2 }),
        f(mirror, { 1 }),
        f(mirror, { 2 }),
      }
    )
  ),
}
