-- Keymaps are automatically loaded on the VeryLazy event
-- Default keymaps that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/keymaps.lua
-- Add any additional keymaps here
local map = vim.keymap.set

map({ "n" }, "<leader>z", "", { desc = "zpecials" })
map({ "n" }, "<leader>zl", "", { desc = "LSP" })
-- this only works if an lsp is attached to the current buffer
map({ "n" }, "<leader>zlr", "<cmd>:LspRestart<cr>", { desc = "Restart" })
map({ "n" }, "<leader>zli", "<cmd>:LspInfo<cr>", { desc = "Info" })
